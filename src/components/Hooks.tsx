import React, { useState } from 'react'

export default function Hooks() {
    const [buttonText, setButtonText] = useState("Click me, please")
    return (
        <div>
            <button onClick={() => setButtonText("Thanks, Button has been clicked :)")}>{buttonText}</button>
        </div>
    )
}
