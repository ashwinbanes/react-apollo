import React, { useState, useEffect } from 'react'

export const TodoForm = ({ saveTodo, setValue }: any) => {

    const [ item, setItem ] = useState()

    return (
        <div className="container-fluid">
            <form className="d-flex flex-column align-items-center justify-content-between" 
            onSubmit={(event) => {
                event.preventDefault()
                saveTodo(item)
            }}>
                <div>
                    <input onChange={(event) => setItem(event.target.value)} type="text" placeholder="Todo Item"/>
                </div>
                <div>
                    <button className="bg-primary text-white btn">Add</button>
                </div>
            </form>
        </div>
    )
}

export default TodoForm
