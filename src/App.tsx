import React, { useState } from 'react';
import './App.css';
import TodoList from './components/TodoList';
import TodoForm from './components/TodoForm';

const App: React.FC = () => {
  const [ todos, setTodos ] = useState(['Enter an item'])
  return (
    <div className="App">
      <TodoForm saveTodo={(todoItem: any) => {
        const trimmedItem = todoItem.trim()
        if(trimmedItem.length > 0) {
          setTodos([...todos, trimmedItem]);
        }
      }} />
      <TodoList todos={todos}
      deleteTodos={(itemIndex: any) => {
        const newTodos = todos.filter((_, index) => index !== itemIndex);
        setTodos(newTodos)
      }}/>
    </div>
  );
}

export default App;
